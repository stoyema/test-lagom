package com.example.hellostream.api

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}

/**
  * The test-lagom stream interface.
  *
  * This describes everything that Lagom needs to know about how to serve and
  * consume the TestlagomStream service.
  */
trait TestlagomStreamService extends Service {

  def stream: ServiceCall[Source[String, NotUsed], Source[String, NotUsed]]

  def check: ServiceCall[NotUsed, String]
  def fwd: ServiceCall[NotUsed, String]

  override final def descriptor = {
    import Service._

    named("test-lagom-stream")
      .withCalls(
        pathCall("/api/b/check", check _),
        pathCall("/api/b/fwd", fwd _),
        namedCall("stream", stream)
      ).withAutoAcl(true)
  }
}

