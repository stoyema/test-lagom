package com.example.hellostream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.example.hellostream.api.TestlagomStreamService
import com.example.hello.api.TestlagomService
import com.lightbend.lagom.scaladsl.client.LagomServiceClientComponents
import com.softwaremill.macwire._

class TestlagomStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TestlagomStreamApplication(context) {
      override def serviceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TestlagomStreamApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[TestlagomStreamService]
  )
}

abstract class TestlagomStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
  with LagomServiceClientComponents
{

  // Bind the TestlagomService client
  lazy val testlagomService = serviceClient.implement[TestlagomService]
  // Bind the service that this server provides
  override lazy val lagomServer = serverFor[TestlagomStreamService](wire[TestlagomStreamServiceImpl])
}
