package com.example.hellostream.impl

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.example.hellostream.api.TestlagomStreamService
import com.example.hello.api.TestlagomService
import com.lightbend.lagom.scaladsl.server.ServerServiceCall

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Implementation of the TestlagomStreamService.
  */
class TestlagomStreamServiceImpl(testlagomService: TestlagomService) extends TestlagomStreamService {

  override def fwd = ServiceCall { _ =>
    testlagomService.check.invoke(NotUsed).map { _ => "WORKED" }
  }

  override def check() = ServiceCall { _ =>
    Future[String] { "TestlagomStreamService GOOD" }
  }

  def stream = ServiceCall { hellos =>
    Future.successful(hellos.mapAsync(8)(testlagomService.hello(_).invoke()))
  }
}
