package com.example.hello.impl

import akka.NotUsed
import com.example.hello.api
import com.example.hello.api.TestlagomService
import com.example.hellostream.api.TestlagomStreamService
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry}
import com.lightbend.lagom.scaladsl.server.ServerServiceCall

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Implementation of the TestlagomService.
  */
class TestlagomServiceImpl(persistentEntityRegistry: PersistentEntityRegistry, streamService: TestlagomStreamService) extends TestlagomService {


  override def fwd: ServiceCall[NotUsed, String] = ServerServiceCall { _ =>
    streamService.check.invoke(NotUsed).map { _ => "WORKED" }
  }

  override def check() = ServerServiceCall { _ =>
    Future[String] { "TestlagomService GOOD" }
  }

  override def hello(id: String) = ServerServiceCall { _ =>
    // Look up the test-lagom entity for the given ID.
    val ref = persistentEntityRegistry.refFor[TestlagomEntity](id)

    // Ask the entity the Hello command.
    ref.ask(Hello(id))
  }

  override def useGreeting(id: String) = ServiceCall { request =>
    // Look up the test-lagom entity for the given ID.
    val ref = persistentEntityRegistry.refFor[TestlagomEntity](id)

    // Tell the entity to use the greeting message specified.
    ref.ask(UseGreetingMessage(request.message))
  }


  override def greetingsTopic(): Topic[api.GreetingMessageChanged] =
    TopicProducer.singleStreamWithOffset {
      fromOffset =>
        persistentEntityRegistry.eventStream(TestlagomEvent.Tag, fromOffset)
          .map(ev => (convertEvent(ev), ev.offset))
    }

  private def convertEvent(helloEvent: EventStreamElement[TestlagomEvent]): api.GreetingMessageChanged = {
    helloEvent.event match {
      case GreetingMessageChanged(msg) => api.GreetingMessageChanged(helloEvent.entityId, msg)
    }
  }
}
