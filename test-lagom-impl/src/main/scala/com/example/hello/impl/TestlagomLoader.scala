package com.example.hello.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.example.hello.api.TestlagomService
import com.example.hellostream.api.TestlagomStreamService
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.client.LagomServiceClientComponents
import com.softwaremill.macwire._

class TestlagomLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new TestlagomApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new TestlagomApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[TestlagomService]
  )
}

abstract class TestlagomApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with CassandraPersistenceComponents
    with LagomKafkaComponents
    with AhcWSComponents
    with LagomServiceClientComponents
{

  // Bind the service that this server provides
  lazy val streamService = serviceClient.implement[TestlagomStreamService]
  override lazy val lagomServer = serverFor[TestlagomService](wire[TestlagomServiceImpl])

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry = TestlagomSerializerRegistry

  // Register the test-lagom persistent entity
  persistentEntityRegistry.register(wire[TestlagomEntity])
}
